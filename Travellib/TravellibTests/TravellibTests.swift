//
//  TravellibTests.swift
//  TravellibTests
//
//  Created by jakobsbode on 23.12.18.
//  Copyright © 2018 jakobsbode. All rights reserved.
//

import XCTest
@testable import Travellib

class TravellibTests: XCTestCase {
    
    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testExample() {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct results.
    }
    
    func testCrypto() {
        testSHA256(input: "123", output: "a665a45920422f9d417e4867efdc4fb8a04a1f3fff1fa07e998e86f7f7a27ae3")
        testSplit()
    }
    
    func testSHA256(input: String, output: String) {
        var data = Crypto.sha256raw(string: input)
        XCTAssert(data.map { String(format: "%02hhx", $0) }.joined() == output)
    }
    
    func testSplit() {
        let expectedResult1: [UInt64] = [0,1,0,1,0,1,0,1]
        let result1 = Crypto.split(data: Data(bytes: [0b01010101]), bitsPerBlock: 1)
        XCTAssert(expectedResult1 == result1)
        let expectedResult2: [UInt64] = [0b01010111, 0b01111000, 0b00000000, 0b11111111]
        let result2 = Crypto.split(data: Data(bytes: [0b01010111, 0b01111000, 0b00000000, 0b11111111]), bitsPerBlock: 8)
        XCTAssert(expectedResult2 == result2)
        let expectedResult3: [UInt64] = [0b000101110111, 0b100000010000]
        let result3 = Crypto.split(data: Data(bytes: [0b00010111, 0b01111000, 0b00010000]), bitsPerBlock: 12)
        XCTAssert(expectedResult3 == result3)
        let expectedResult4: [UInt64] = [0b01010111011110, 0b00010100001111]
        let result4 = Crypto.split(data: Data(bytes: [0b01010111, 0b01111000, 0b01010000, 0b11111111]), bitsPerBlock: 14)
        XCTAssert(expectedResult4 == result4)
        let expectedResult5: [UInt64] = [0b010,0b101]
        let result5 = Crypto.split(data: Data(bytes: [0b01010101]), bitsPerBlock: 3)
        XCTAssert(expectedResult5 == result5)
        let expectedResult6: [UInt64] = [0b01, 0b011, 0b1000, 0b111001011100100101, 0b11101]
        let result6 = Crypto.split(data: Data(bytes: [0b01011100, 0b01110010, 0b11100100, 0b10111101]), bitsPerBlock: [2,3,4,18,5])
        XCTAssert(expectedResult6 == result6)
        let expectedResult7: [UInt64] = [0b01, 0b011, 0b1000, 0b111001011100100101]
        let result7 = Crypto.split(data: Data(bytes: [0b01011100, 0b01110010, 0b11100100, 0b10111101]), bitsPerBlock: [2,3,4,18,50])
        XCTAssert(expectedResult7 == result7)
    }
    
    func testPolynom() {
//        0    14
//        1    6
//        2    12
//        3    7
//        4    10
//        5    8
//        6    13
//        7    12
//        8    12
//        9    7
//        10   13
//        11   5
//        12   7
//        13   6
//        14   3
//        15   1

        var p = Crypto.Polynom(coeficients: [0b1110, 0b011, 0b101, 0b1110], irreducible: 0b11101)
        XCTAssert(p.evaluate(at: 0) == 14)
        XCTAssert(p.evaluate(at: 1) == 6)
        XCTAssert(p.evaluate(at: 2) == 12)
        XCTAssert(p.evaluate(at: 3) == 7)
        XCTAssert(p.evaluate(at: 4) == 10)
        XCTAssert(p.evaluate(at: 5) == 8)
        XCTAssert(p.evaluate(at: 6) == 13)
        XCTAssert(p.evaluate(at: 7) == 12)
        XCTAssert(p.evaluate(at: 8) == 12)
        XCTAssert(p.evaluate(at: 9) == 7)
        XCTAssert(p.evaluate(at: 10) == 13)
        XCTAssert(p.evaluate(at: 11) == 5)
        XCTAssert(p.evaluate(at: 12) == 7)
        XCTAssert(p.evaluate(at: 13) == 6)
        XCTAssert(p.evaluate(at: 14) == 3)
        XCTAssert(p.evaluate(at: 15) == 1)
    }
    
    func testPerformanceExample() {
        // This is an example of a performance test case.
        self.measure {
            // Put the code you want to measure the time of here.
        }
    }
    
}
