//
//  Traveler.swift
//  Travellib
//
//  Created by jakobsbode on 23.12.18.
//  Copyright © 2018 jakobsbode. All rights reserved.
//

import Foundation

public class Traveler: Codable {
    
    private var _contacts: [String] = []
    private var _keys: [String : String] = [:]
    private var salts: [String : String] = [:]
    private var _backupAddress: String = ""
    private var _backupKey: String = ""
    private var _canSecurelyBackup = true
    private var pw1 = ""
    private var pw2 = ""
    
    public var contacts: [String] {
        get {
            let result = _contacts
            return result
        }
    }
    
    public var keys: [String: String] {
        get {
            let result = _keys
            return result
        }
    }
    
    public var backupAddress: String {
        get {
            let result = _backupAddress
            return result
        }
    }
    
    public var backupKey: String {
        get {
            let result = _backupKey
            return result
        }
    }
    
    public var canSecurelyBackup: Bool {
        get {
            let result = _canSecurelyBackup
            return result
        }
    }
    
    /// The alphabet used to create a password from.
    /// It's default value is the alphabet
    /// abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789-/:;()$&@".,?!'[]{}#%^*+=_\|~<>€¥£•
    public let alphabet: [String] = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789-/:;()$&@\".,?!'[]{}#%^*+=_\\|~<>€¥£•".map { String($0) }
    fileprivate let attackerProbability = 3/pow(10, 4.25)
    
    private static func parsePassword(password: String, alphabetCount: Int, errorDetectionBits: Int = 6) throws -> (String, String, String, String) {
        let pw2Length = Int(ceil(log(pow(2, 18))/log(Double(alphabetCount))))
        let errorDetectionLength = Int(ceil(log(pow(2, Double(errorDetectionBits)))/log(Double(alphabetCount))))
        if alphabetCount == 0 || password.count <= 2*pw2Length {
            throw TravelError.corruptData
        }
        let errorDetection = password.suffix(errorDetectionLength)
        let encodedData = password.dropLast(errorDetectionLength)
        
        let pw2 = encodedData.prefix(pw2Length)
        let y = encodedData.dropFirst(pw2Length).prefix(pw2Length)
        let pw1 = encodedData.dropFirst(2*pw2Length)
        
        return (String(pw2), String(y), String(pw1), String(errorDetection))
    }
    
    /// loads a Traveler from stored data.
    /// - important: check if the keys stored in the applications keymanagement are equal to the ones stored in the returned Traveler
    public static func load(from data: Data, password: String, errorDetectionBits: Int = 6) throws -> Traveler {
        //create Checksum, verify it with the one in the password and throw invalidData Error, if they don't match up
        //otherwise set password in Traveler-object and succed
        let traveler = try JSONDecoder().decode(Traveler.self, from: data)
        let alphabetCount = traveler.alphabet.count
        let passwordTuple = try parsePassword(password: password, alphabetCount: alphabetCount, errorDetectionBits: errorDetectionBits)
        let pw2 = passwordTuple.0
        let y = passwordTuple.1
        let pw1 = passwordTuple.2
        let errorDetection = passwordTuple.3
        traveler.setPassword(pw1: pw1, pw2: pw2)
        if traveler.calculateErrorDetection(of: pw2+y+pw1, bits: errorDetectionBits) != errorDetection {
            throw TravelError.invalidPassword
        }
        if try traveler.createChecksum() != y {
            throw TravelError.corruptData
        }
        return traveler
    }
    
    /// - important: Only call if the wrong or no password is given by the user (e.g. if users device is compromised). Do not trust the returned Taveler, since it may be compromised!
    /// - Returns: a insecure Traveler. Do not trust it's attributes values!
    public static func loadInsecure(from data: Data) -> Traveler {
        do {
            let traveler = try JSONDecoder().decode(Traveler.self, from: data)
            traveler._canSecurelyBackup = false
            return traveler
        } catch {
            return Traveler(contacts: [], keys: [:], backupAddress: "", backupKey: "", canSecurelyBackup: false)
        }
    }
    
    /// creates a Traveler object.
    /// - important: no backup is created in Traveler. The backup parameters are only used to verify, no modification to this parameters happend by an adversary while crossing the border.
    /// - Parameter contacts: list of contact identifiers
    /// - Parameter keys: dictionary of keys used to create a secure communication with a contact. It is expected, that the values outof the parameter contacts are used as the dictionary-keys.
    /// - Parameter backupAddress: the address a backup should be sent to.
    /// - Parameter backupKey: the key used to encrypt the backup.
    /// - Parameter canSecurelyBackup: states if it was previously possible to create securly backups i.e. no compromise happend before.
    public init(contacts: [String], keys: [String : String], backupAddress: String, backupKey: String, canSecurelyBackup: Bool) {
        self._contacts = contacts
        self._keys = keys
        self._backupAddress = backupAddress
        self._backupKey = backupKey
        salts = [:]
        do {
            try createSalts()
            try createPasswords()
        } catch {
            print("error in salts")
            //TODO: throw Error here, that is thrown in every other programmer callable function too, if something wents wrong; we assume different Errors cannot be solved by the programmer. that means they have to be userhandled
            //Give the user only the option to delete his keys and communication, if an Error occurs. Different errors would be confusing, since they require a deeper knowlege of the mechanism used.
        }
    }
    
    private func createSalts() throws {
        var salt = ""
        for c in _contacts {
            salt = Crypto.convertToString(from: try Crypto.createRandom(bytes: 32))
            salts[c] = salt
        }
    }
    
    private func createPasswords() throws {
        var characters = 3
        let numberContacts = Double(_contacts.count)
        for i in 1 ... _contacts.count {
            if attackerProbability > 1/pow(2, 18) + numberContacts / pow(Double(alphabet.count), Double(i)) {
                characters = i
                break
            }
        }
        pw1 = try Crypto.createRandom(characters: characters, alphabet: alphabet)
        pw2 = try Crypto.createRandom(bits: 18, alphabet: alphabet)
    }
    
    private func sha256(string: String) -> String {
        let digestData = Crypto.sha256raw(string: string)
        return Crypto.convertToString(from: digestData)
    }
    
    private func createChecksum() throws -> String {
        var input = ""
        for c in _contacts {
            if let salt = salts[c], let key = _keys[c] {
                input.append("(")
                input.append(c)
                input.append(",")
                input.append(salt)
                input.append(",")
                input.append(key)
                input.append(")")
            } else {
                throw TravelError.invalidData
            }
        }
        input.append("("+_backupAddress+","+_backupKey+")")
        input.append(String(_canSecurelyBackup))
        let checksum = Crypto.sha256raw(string: input)
        let coefficients = Crypto.split(data: checksum, bitsPerBlock: 18) //TODO: handle case where all coefficients are zero
        let polynom = Crypto.Polynom.init(coeficients: coefficients, irreducible: (1 << 18)+(1 << 3)) //chosen irreducible from http://www.hpl.hp.com/techreports/98/HPL-98-135.pdf
        let y = polynom.evaluate(at: try Crypto.convertToNumber(password: pw1, alphabet: alphabet))
        return try Crypto.convertToPassword(number: y, alphabet: alphabet, bits: 18)
    }
    
    //calculates and encodes an errordetection code over text. Is capable to use up to 256 bits.
    private func calculateErrorDetection(of text: String, bits: Int) -> String {
        let hash = Crypto.sha256raw(string: text)
        let errorDetection = Crypto.split(data: hash, bitsPerBlock: bits)
        if let errorDetection = errorDetection.first, let result = try? Crypto.convertToPassword(number: errorDetection, alphabet: alphabet, bits: bits) {
            return result
        }
        return ""
    }
    
    fileprivate func setPassword(pw1: String, pw2: String) {
        self.pw1 = pw1
        self.pw2 = pw2
    }
    
    /// - Parameter errorDetectionBits: the number of bits used for errordetection. The maximal supported value is 256.
    /// - Returns: the password the user has to remember.
    public func getToRemember(errorDetectionBits: Int = 6) throws -> String {
        let y = try createChecksum()
        let password = pw2+y+pw1
        let errorDetection = calculateErrorDetection(of: password, bits: errorDetectionBits)
        return password+errorDetection
    }
    
    /// - Returns: the shared secrets which should be send to the contacts ordered by contacts
    public func getToSend() throws -> [String : String] {
        var result: [String: String] = [:]
        for c in _contacts {
            let secret = Crypto.sha256(string: pw1+salts[c]!)
            result[c] = secret
        }
        return result
    }
    
    /// - important: Call this function after getPassword and getSharedSecrets. It will delete the password stored in Traveler.
    /// - Returns: The data the using application should store while crossing the border
    public func getToStore() -> Data {
        deletePasswordReference()
        let data = try? JSONEncoder().encode(self)
        return data!
    }
    
    private func deletePasswordReference() {
        pw1 = ""
        pw2 = ""
    }
}
