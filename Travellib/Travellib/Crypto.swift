//
//  Crypto.swift
//  Travellib
//
//  Created by jakobsbode on 23.12.18.
//  Copyright © 2018 jakobsbode. All rights reserved.
//

import Foundation
import CommonCrypto


internal class Crypto {
    
    public class Polynom {
        
        public typealias PolynomGF2 = UInt64
        
        var irreducible: PolynomGF2
        var coefficients: [PolynomGF2] = []
        public let degree: Int
        
        public init(coeficients: [PolynomGF2], irreducible: PolynomGF2) {
            self.irreducible = irreducible
            degree = coeficients.count-1
            self.coefficients = []
            for c in coeficients {
                self.coefficients.append(modulo(p: c, q: irreducible))
            }
        }
        
        internal static func degree(polynom: PolynomGF2) -> Int {
            //        for i in 63 ... 0 {
            //            if polynom & (1 << i) != 0 {
            //                return i
            //            }
            //        }
            //        return 0
            var degree = 63-polynom.leadingZeroBitCount
            if degree < 0 {
                degree = 0
            }
            return degree
        }
        
        internal func add(p: PolynomGF2, q: PolynomGF2) -> PolynomGF2 {
            return p ^ q
        }
        
        /*
         requires 'a' to have a degree less or equal to self.degree
         */
        internal func multiply(p: PolynomGF2, q: PolynomGF2) -> PolynomGF2 {
            let degreeIrreducible = Polynom.degree(polynom: irreducible)
            var result: PolynomGF2 = 0
            for i in 0 ... 63 {
                result = result << 1
                if q & (1 << (63-i)) != 0 {
                    result = add(p: result, q: p)
                }
                if result & (1 << degreeIrreducible) != 0 {
                    result = add(p: result, q: irreducible)
                }
            }
            return result
        }
        
        //    internal func power(p: PolynomGF2, exponent: Int) -> PolynomGF2 {
        //        var result: PolynomGF2 = 1
        //        for _ in 1 ... exponent {
        //            result = multiply(p: result, q: p)
        //        }
        //        return result
        //    }
        
        internal func modulo(p: PolynomGF2, q: PolynomGF2) -> PolynomGF2 {
            let degreeQ = Polynom.degree(polynom: q)
            var temp = p
            var degreeT = Polynom.degree(polynom: temp)
            while (degreeT >= degreeQ) {
                temp = add(p: temp, q: (q << (degreeT-degreeQ)))
                degreeT = Polynom.degree(polynom: temp)
            }
            return temp
        }
        
        public func evaluate(at x: PolynomGF2) -> PolynomGF2 {
            var power: PolynomGF2 = 1
            var result: PolynomGF2 = 0
            let input = modulo(p: x, q: irreducible)
            for i in 0 ... degree {
                let monom = multiply(p: coefficients[degree-i], q: power)
                result = add(p: result, q: monom)
                power = multiply(p: power, q: input)
            }
            return result
        }
    }
    
    //just for testing
    public static func convertToString(from data: Data) -> String {
        return data.map { String(format: "%02hhx", $0) }.joined()
    }
    
    public static func sha256raw(string: String) -> Data {
        let messageData = string.data(using:.utf8)!
        var digestData = Data(count: Int(CC_SHA256_DIGEST_LENGTH))
        
        _ = digestData.withUnsafeMutableBytes {digestBytes in
            messageData.withUnsafeBytes {messageBytes in
                CC_SHA256(messageBytes, CC_LONG(messageData.count), digestBytes)
            }
        }

        return digestData
    }
    
    public static func sha256(string: String) -> String {
        let digestData = sha256raw(string: string)
        return convertToString(from: digestData)
    }
    
    //TODO: delete Data after usage
    /*
     Create random data. 'bytes' encodes the number of bytes in the random data.
     */
    public static func createRandom(bytes: Int) throws -> Data {
        var data = Data.init(count: bytes)
        try _ = data.withUnsafeMutableBytes { (pointer: UnsafeMutablePointer<UInt8>) throws in
            let rawPointer = UnsafeMutableRawPointer(pointer)
            let status = SecRandomCopyBytes(kSecRandomDefault, bytes, rawPointer)
            if status != errSecSuccess {
                throw TravelError.cannotCreateRandom
            }
        }
        return data
    }
    
    /*
     Splits data into blocks of size bitsPerBlock.
     The blocks are created from first bit in data to the last bit.
     If bitsPerBlock do not divides the number of bits in data without a remainder r, the last r bits will be leftout when creating the blocks.
     */
    public static func split(data: Data, bitsPerBlock: Int) -> [UInt64] {
        var result: [UInt64] = []
        _ = data.withUnsafeBytes { (start: UnsafePointer<UInt8>) in
            var pointer = start
            var carryBits = 8
            for _ in 0 ..< 8*data.startIndex.distance(to: data.endIndex)/bitsPerBlock {
                if carryBits <= 0 {
                    carryBits = 8
                }
                var blockBits = bitsPerBlock
                var value: UInt64 = 0
                if carryBits < 8 {
                    if blockBits < carryBits {
                        value += UInt64((pointer.pointee) % (UInt8(1) << (carryBits))) >> (carryBits-blockBits)
                        carryBits -= blockBits
                        result.append(value)
                        continue
                    }
                    value += UInt64((pointer.pointee) % (UInt8(1) << (carryBits))) << (blockBits-carryBits)
                    blockBits -= carryBits
                    pointer = pointer.advanced(by: 1)
                    carryBits = 8
                }
                while blockBits >= 8 {
                    value += UInt64(pointer.pointee) << (blockBits-8)
                    blockBits -= 8
                    pointer = pointer.advanced(by: 1)
                }
                if blockBits != 0 {
                    //let pointerValue = pointer.pointee //TODO: remove
                    value += UInt64(pointer.pointee >> (carryBits-blockBits))
                    carryBits = 8-blockBits
                }
                result.append(value)
            }
        }
        return result
    }
    
    /*
     Splits data into bitsPerBlock.count blocks. bitsPerBlock encode the size (number of bits) of the blocks.
     The blocks are created from first bit in data to the last bit.
     If the size of the blocks is bigger than the number of bits in data, only the first blocks, that have - summed up - a smaller number of bits than data, are created.
     */
    public static func split(data: Data, bitsPerBlock: [Int]) -> [UInt64] {
        var result: [UInt64] = []
        _ = data.withUnsafeBytes { (start: UnsafePointer<UInt8>) in
            var pointer = start
            var carryBits = 8
            var allProcessedBits = 0
            for i in 0 ..< bitsPerBlock.count {
                if carryBits <= 0 {
                    carryBits = 8
                }
                var blockBits = bitsPerBlock[i]
                if blockBits+allProcessedBits > 8*data.startIndex.distance(to: data.endIndex) {
                    break
                }
                allProcessedBits += blockBits
                var value: UInt64 = 0
                if carryBits < 8 {
                    if blockBits < carryBits {
                        value += UInt64((pointer.pointee) % (UInt8(1) << (carryBits))) >> (carryBits-blockBits)
                        carryBits -= blockBits
                        result.append(value)
                        continue
                    }
                    value += UInt64((pointer.pointee) % (UInt8(1) << (carryBits))) << (blockBits-carryBits)
                    blockBits -= carryBits
                    pointer = pointer.advanced(by: 1)
                    carryBits = 8
                }
                while blockBits >= 8 {
                    value += UInt64(pointer.pointee) << (blockBits-8)
                    blockBits -= 8
                    pointer = pointer.advanced(by: 1)
                }
                if blockBits != 0 {
                    //let pointerValue = pointer.pointee //TODO: remove
                    value += UInt64(pointer.pointee >> (carryBits-blockBits))
                    carryBits = 8-blockBits
                }
                result.append(value)
            }
        }
        return result
    }
    
    /*
     requires alphabet to have a maximal length of 256
     */
    public static func createRandom(characters: Int, alphabet: [String]) throws -> String {
        guard alphabet.count <= 256 else {
            throw TravelError.invalidAlphabet
        }
        func createRandomCharacter(alphabet: [String]) throws -> String {
            var d = try Crypto.createRandom(bytes: 1)
            while d.last! > alphabet.count-1 {
                try d = Crypto.createRandom(bytes: 1)
            }
            return alphabet[Int(d.last!)]
        }
        
        var result = ""
        for _ in 0..<characters {
            result.append(try createRandomCharacter(alphabet: alphabet))
        }
        return result
    }
    
    //bits should be less or equal to 64, more bits will not be considered at the moment
    //alphabet should contian maximal 256 symbols at the moment
    public static func createRandom(bits: Int, alphabet: [String]) throws -> String {
        guard alphabet.count <= 256 else {
            throw TravelError.invalidAlphabet
        }
        guard bits <= 64 else {
            throw TravelError.tooManyBits
        }
        let randomNumber = try Crypto.split(data: Crypto.createRandom(bytes: (bits/8+(bits%8 != 0 ? 1: 0))), bitsPerBlock: bits)[0]
        let result = try Crypto.convertToPassword(number: randomNumber, alphabet: alphabet, bits: bits)
        return result
    }
    
    public static func convertToPassword(number: UInt64, alphabet: [String], bits: Int) throws -> String {
        guard alphabet.count <= 256 else {
            throw TravelError.invalidAlphabet
        }
        guard bits <= 63 else {
            throw TravelError.tooManyBits
        }
        var result = ""
        var n = number
        var max: UInt64 = (1 << bits)
        max = max-1
        while max != 0 {
            let char = n % UInt64(alphabet.count)
            result.append(alphabet[Int(char)])
            n = n / UInt64(alphabet.count)
            max = max / UInt64(alphabet.count)
        }
        return result
    }
    
    public static func convertToNumber(password: String, alphabet: [String]) throws -> UInt64 {
        var result: UInt64 = 0
        var pw = password
        for _ in 0 ..< pw.count {
            let c = pw.removeLast()
            if let index = alphabet.index(of: String(c)) {
                result = result * UInt64(alphabet.count) + UInt64(index)
            } else {
                throw TravelError.invalidAlphabet
            }
            
        }
        return result
    }
}
