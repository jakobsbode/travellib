//
//  Travellib.h
//  Travellib
//
//  Created by jakobsbode on 23.12.18.
//  Copyright © 2018 jakobsbode. All rights reserved.
//

#import <UIKit/UIKit.h>

//! Project version number for Travellib.
FOUNDATION_EXPORT double TravellibVersionNumber;

//! Project version string for Travellib.
FOUNDATION_EXPORT const unsigned char TravellibVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <Travellib/PublicHeader.h>


