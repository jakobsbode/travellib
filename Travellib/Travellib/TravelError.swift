//
//  TravelError.swift
//  Travellib
//
//  Created by jakobsbode on 23.12.18.
//  Copyright © 2018 jakobsbode. All rights reserved.
//

import Foundation

public enum TravelError: Error {
    case cannotCreateRandom, invalidAlphabet, tooManyBits, invalidData, corruptData, invalidPassword
}
